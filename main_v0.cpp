////////////////////////////////////////////////////////////////////////
// OOP Tutorial Exploring Ralationships between Dice and RandomNumberGenerator classes (version 0: to be modified)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	//for time
#include <iostream>	//for cin >> and cout <<
using namespace std;

//--------RandomNumberGenerator class
class RandomNumberGenerator
{
	public:
		RandomNumberGenerator();
		~RandomNumberGenerator();
		int getRandomValue( int);
	private:
		void seed();
};
RandomNumberGenerator::RandomNumberGenerator() {
	seed();	//reset the random number generator from current system time
	cout << "\nCalling RandomNumberGenerator() - default constructor... ";
}
RandomNumberGenerator::~RandomNumberGenerator() {
	cout << "\nCalling RandomNumberGenerator() - destructor... ";
}
void RandomNumberGenerator::seed() {
	srand(static_cast<unsigned int>(time(0)));
}
int RandomNumberGenerator::getRandomValue( int max) {
	return ( rand() % max) + 1; //produce a random number in range [1..max]
}
//--------end of RandomNumberGenerator class

//--------Dice class
class Dice
{
	public:
		Dice();
		int getFace() const;
		void roll();
	private:
		int face_;	//number on dice
		static RandomNumberGenerator srng_; 	//internal RandomNumberGenerator
};
RandomNumberGenerator Dice::srng_; 	//static RandomNumberGenerator
Dice::Dice() 
: face_( 0)
{
	cout << "\nCalling Dice() - default constructor...";
}
int Dice::getFace() const {
	return face_;	//get value of dice face
}
void Dice::roll() {
	face_ = srng_.getRandomValue( 6); 	//roll dice
}
//--------end of Dice class

//--------client code
int main()
{
	Dice dice1;	//line1
	Dice dice2;
	int score( 0);

	cout << "\n\nStarting Score: " << score << "\n";
	for ( int i(1); i <= 10; ++i)
	{
		dice1.roll();	//line2
		dice2.roll();
		cout << "\ndice1: " << dice1.getFace()
		     << " and dice2: " << dice2.getFace();
		if (dice1.getFace() == dice2.getFace())
			++score;
	}
	cout << "\n\nFinal Score: " << score << "\n\n";
	system ("pause"); //hold screen
	return 0;
}
