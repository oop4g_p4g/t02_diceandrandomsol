////////////////////////////////////////////////////////////////////////
// OOP Tutorial Exploring Ralationships between Dice and RandomNumberGenerator classes (version 0: to be modified)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>
#include <iostream>
using namespace std;

//--------RandomNumberGenerator class
class RandomNumberGenerator
{
public:
	RandomNumberGenerator();
	~RandomNumberGenerator();
	int getRandomValue(int) const;
private:
	void seed() const;
};
RandomNumberGenerator::RandomNumberGenerator() {
	seed();	//reset the random number generator from current system time
	cout << "\nCalling RandomNumberGenerator() - default constructor... ";
}
RandomNumberGenerator::~RandomNumberGenerator() {
	cout << "\nCalling RandomNumberGenerator() - destructor... ";
}
int RandomNumberGenerator::getRandomValue(int max) const {
	return (rand() % max) + 1; //produce a random number in range [1..max]
}
void RandomNumberGenerator::seed() const {
	srand(static_cast<unsigned int>(time(0)));
}
//--------end of RandomNumberGenerator class

//--------Dice class
class Dice
{
public:
	Dice();
	~Dice();
	int getFace() const;
	void roll();
private:
	int face_;	//number on dice
	RandomNumberGenerator rng_; 	//internal RandomNumberGenerator
};
Dice::Dice() : face_(0), rng_() {
	cout << "\nCalling Dice() - default constructor...";
}
Dice::~Dice() {
	cout << "\nCalling ~Dice() - destructor...";
}
int Dice::getFace() const {
	return face_;	//get value of dice face
}
void Dice::roll() {
	face_ = rng_.getRandomValue(6); 	//roll dice
}
//--------end of Dice class

//--------client code
int main()
{
	Dice dice1;	//line1
	Dice dice2;
	int score(0);

	cout << "\n\nStarting Score: " << score << "\n";
	for (int i(1); i <= 10; ++i)
	{
		dice1.roll();	//line2
		dice2.roll();
		cout << "\ndice1: " << dice1.getFace()
			<< " and dice2: " << dice2.getFace();
		if (dice1.getFace() == dice2.getFace())
			++score;
	}
	cout << "\nFinal Score: " << score << "\n";
	system("pause"); //hold screen
	return 0;
}
